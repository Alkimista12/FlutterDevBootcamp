import 'package:flutter/material.dart';

import 'package:helloworld/gradient_container.dart';

var colorsList = [Colors.black, Colors.white30];

void main() {
  runApp(
    MaterialApp(
        home: Scaffold(
          body: GradientContainer(colorsList),
        ),
        debugShowCheckedModeBanner: false),
  );
}
