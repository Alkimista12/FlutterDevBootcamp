import 'package:flutter/material.dart';
import 'dart:math';

final ranbdomizer = Random();

class DiceRoller extends StatefulWidget {
  const DiceRoller({super.key});

  @override
  State<DiceRoller> createState() {
    return _DiceRollerState();
  }
}

class _DiceRollerState extends State<DiceRoller> {
  var activeDiceImage = 'assets/images/Dice-1.png';

  void rollDice() {
    var diceNum = ranbdomizer.nextInt(6) + 1;
    setState(() {
      activeDiceImage = 'assets/images/Dice-$diceNum.png';
    });

    print('Button Clicked number - $diceNum');
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ClipRRect(
            borderRadius: BorderRadius.circular(28),
            child: Image.asset(
              activeDiceImage,
              width: 200,
            )),
        const SizedBox(height: 30),
        TextButton(
          onPressed: rollDice,
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(30.0),
            foregroundColor: Colors.black,
          ),
          child: const Text("Roll Dice", style: TextStyle(fontSize: 30)),
        )
      ],
    );
  }
}
